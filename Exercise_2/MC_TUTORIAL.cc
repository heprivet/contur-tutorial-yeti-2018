// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class MC_TUTORIAL : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_TUTORIAL);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      // Initialise and register projections
      const FinalState fs;
      declare(FinalState(Cuts::abseta < 5 && Cuts::pT > 100*MeV), "fs");

      
      
      FastJets fj04(fs,  FastJets::ANTIKT, 0.4, JetAlg::NO_MUONS, JetAlg::NO_INVISIBLES);
      //fj04.useInvisibles();
      declare(fj04, "AntiKT04");

      // Book histograms
      _h1["jet1pt"] = bookHisto1D("jet1pt", 20, 0.0, 200.0);

    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {

      const double weight = event.weight();
      
      /// @todo Do the event by event analysis here
      Jets jets = apply<FastJets>(event, "AntiKT04").jetsByPt(Cuts::pT > 25*GeV && Cuts::absrap < 4.4);

      double jpt1 = 0.0;

      if (jets.size()){
	jpt1=jets[0].pT()/GeV;
      }

      _h1["jet1pt"]->fill(jpt1,weight);
      
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      const double sf(crossSection() / sumOfWeights());
      for (HistoMap1D::value_type& hist : _h1) { scale(hist.second, sf); }

      

    }

    //@}

  private:
    typedef map<string, Histo1DPtr> HistoMap1D;
    HistoMap1D _h1;

  };

  

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_TUTORIAL);


}
