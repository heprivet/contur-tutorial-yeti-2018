BEGIN PLOT /MC_TUTORIAL/jet1pt
Title=Leading Jet $p_{T}$, AntiKT 0.4 Jet
XLabel=$p_{T}$ [GeV]
YLabel=$\frac{d\sigma}{dp_{T}}$ [GeV$^{-1}$]
# + any additional plot settings you might like, see make-plots documentation
END PLOT

# ... add more histograms as you need them ...
