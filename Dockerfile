FROM hepstore/rivet-herwig:2.6.1-7.1.3
MAINTAINER David Yallup <dyallup@cern.ch>

CMD /bin/bash

RUN dnf install -y mercurial scipy sqlite-devel nano emacs texlive

RUN pip install configobj

RUN hg clone https://bitbucket.org/heprivet/contur /contur

#ENV DISPLAY :0

WORKDIR /contur

